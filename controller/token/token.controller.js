const {USERNAME,PASSWORD} = require('../../constants/config')
const {StatusCodes, getReasonPhrase}  = require( "http-status-codes") 
const {genToken} = require("../../utils/auth")
const authModel = require('../../models/auth/auth.model')()
const getToken = async(req,res)=>{
    try {
        const {username,password} = req.body

        if(!username || !password){
            return res.status(StatusCodes.BAD_REQUEST).json({code:StatusCodes.BAD_REQUEST , message:"USERNAME OR PASSWORD IS REQUIRE"})

        }
        if( username !== USERNAME  || password !== PASSWORD ){
            return res.status(StatusCodes.BAD_REQUEST).json({code:StatusCodes.BAD_REQUEST , message:"USERNAME OR PASSWORD IS INVALID"})
        }
        const token = await genToken(username)
        const auth = {username,token}
        const created  = await authModel.create(auth)
        return res.status(StatusCodes.OK).json({code:StatusCodes.OK,message:getReasonPhrase(StatusCodes.OK),token:token})
    } catch (error) {
        console.log(error)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({code:StatusCodes.INTERNAL_SERVER_ERROR , message:getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)})
    
    }
}

module.exports = {
    getToken
}