const {StatusCodes, getReasonPhrase}  = require( "http-status-codes") 
const customerModel = require("../../models/customer/customer.model")()
const findAll = async(req,res)=>{
    try {
        return res.status(StatusCodes.OK).json({code:StatusCodes.OK,message:"This is all customers : []"})
    } catch (error) {
        console.log(error);
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({code:StatusCodes.INTERNAL_SERVER_ERROR , message:getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)})
    }
}
const create = async(req,res)=>{
    try {
        const customer = req.body
        await customerModel.create(customer)
        console.log(customer);
        return res.status(StatusCodes.OK).json({code:StatusCodes.OK,message:"created"})

    } catch (error) {
        console.log(error);
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({code:StatusCodes.INTERNAL_SERVER_ERROR , message:getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)})

    }
}

const update = async(req,res)=>{
    try {
        return res.status(StatusCodes.OK).json({code:StatusCodes.OK,message:getReasonPhrase(StatusCodes.OK)})
    } catch (error) {
        console.log(error)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({code:StatusCodes.INTERNAL_SERVER_ERROR , message:getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)})

    }
}

module.exports = {
    update,
    findAll,
    create
}