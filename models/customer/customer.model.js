const db = require("../../db/db_connection")()
module.exports = ()=>{
    const model ={
        async create(customer){
            const result = db.query(`INSERT  INTO customers SET ?`,[customer])
            return result
        }
    }
    return model
}