const db = require("../../db/db_connection")()

module.exports = () =>{
    const model = {
        async create (auth){
            const result = db.query(`INSERT INTO authen SET ?`,[auth])
            return result
        },
        async findOne({token}){
            const result = db.query(`SELECT token,created_on FROM authen WHERE token = ?`,[token])
            return result
        },
        async findTokenByUsername({username}){
            const result = db.query(`SELECT token FROM authen WHERE username = ?`,[username])
            return result
        },
        async updateToken({expired,token}){
            const result = db.query(`UPDATE authen SET expired = ? WHERE token = ?`,[expired,token])
            return result
        },
    }
    return model
}