const express = require('express');
const router = express.Router();
const customer = require("../controller/customers/customer.controller")
const {validateBearer} = require("../middleware/auth")
router.route('/').put([validateBearer],customer.update).get([],customer.findAll).post([],customer.create)
module.exports = router

