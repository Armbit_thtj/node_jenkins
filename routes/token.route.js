const express = require('express');
const router = express.Router();
const auth = require("../controller/token/token.controller")

router.route('/').post([],auth.getToken)

module.exports = router