const {StatusCodes, getReasonPhrase}  = require( "http-status-codes") 
const authModel = require('../models/auth/auth.model')()
const {findOne} = require("../utils/app")
const validateBearer = async(req,res,next)=>{
    try {
        const authorization = req.headers.authorization
        const bearer = authorization.split(" ")[1]
        if(!authorization){
            return res.status(StatusCodes.BAD_REQUEST).json({code:StatusCodes.BAD_REQUEST , message:getReasonPhrase(StatusCodes.BAD_REQUEST)})            
        }
        const hasToken = await authModel.findOne({token:bearer})
        const {created_on,token} = await findOne(hasToken)
        console.log(token);
        const current_date = new Date(Date.now())
        if(!token){
            return res.status(StatusCodes.BAD_REQUEST).json({code:StatusCodes.BAD_REQUEST , message:"INVALID TOKEN"})            
        }
        const diffTime = Math.abs(created_on - current_date)
        const diffDays = Math.round(diffTime / (1000*60*60*24))
        const diffHour = diffTime / (1000*60*60)
        if(diffDays>=1 && diffHour>=24){
            const updated = await authModel.updateToken({expired:true,token})
            return res.status(StatusCodes.BAD_REQUEST).json({code:StatusCodes.BAD_REQUEST , message:"TOKEN EXPIRED"})
            
        }
        return next()
    } catch (error) {
        console.log(error)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({code:StatusCodes.INTERNAL_SERVER_ERROR , message:getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)})

    }
}
module.exports = {
    validateBearer
}