module.exports = {
    port: 3306,
    db: {
        queueLimit: 0,
        connectionLimit: 0,
        host: "localhost",
        user: "root",
        password: "",
        database: "allkons",
        debug: false
    }
}