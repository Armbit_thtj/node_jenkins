const mysql = require('mysql'),
// config = require('./env/config'),
config = require('./env/config'),
util = require('util')

const dbConfig={
    host:config.db.host,
    user:config.db.user,
    password:config.db.password,
    database:config.db.database,
    port:config.port
}

module.exports = ()=>{
    const pool = mysql.createPool(dbConfig)
    pool.query = util.promisify(pool.query)
    return pool
}

