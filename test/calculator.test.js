const calculator = require('../services/calculator')
describe('test sum',()=>{
    test('test 5 + 8', async () => {
        const result = await calculator.add(5,8)
        expect(result).toBe(13);
        expect(result).toBeGreaterThan(10);
        expect(result).toBeGreaterThanOrEqual(13);
        expect(result).toBeLessThan(15);
        expect(result).toBeLessThanOrEqual(15);
    })
})

describe('object assignment',()=>{
    test('assign', async () => {
        const data = {
            one:1
        }
        data.two = 2
        expect(data).toEqual({one:1,two:2});
    })
})

describe('null',()=>{
    test('test null',async()=>{
        const n = null
        expect(n).toBeNull()
        expect(n).toBeDefined()
    })
})

describe('str regex',()=>{
    test('test regex',async()=>{
        expect('Thanachot').toMatch(/nach/)
    })
})

describe('array',()=>{
    test('test array',()=>{
        const list = [
            "a","b","c","d"
        ]
        expect(list).toContain('c')
    })
})