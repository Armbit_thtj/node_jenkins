const jwt = require('jsonwebtoken')
const {SECRET_KEY} = require('../constants/config')
const genToken = async(username)=>{
    const token = await jwt.sign({user:username},SECRET_KEY)
    return token
}

module.exports = {
    genToken
}