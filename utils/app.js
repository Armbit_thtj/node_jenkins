const findOne = async(data)=>{
    return data[0] || []
}

module.exports = {
    findOne
}