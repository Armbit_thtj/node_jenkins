FROM node
FROM node
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install nodemon -g
RUN npm install

COPY . .

EXPOSE 8123

CMD ["npm","start"]